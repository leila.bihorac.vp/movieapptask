//
//  TableController.swift
//  MovieAppTask
//
//  Created by Leila Bihorac on 07/05/2020.
//  Copyright © 2020 Leila Bihorac. All rights reserved.
//

import Foundation
import UIKit

class TableController : UITableViewController {
    
    
    
    @IBOutlet weak var navBarButton: UINavigationItem!
    var searchedText = String()
    var moviesInfo : MoviesJSON!
    var movieList = [Results]()
    @IBOutlet var movieTable: UITableView!
    var no_image_avabile = UIImage(named: "no_image_avabile")
    let defauts = UserDefaults()
    var pageNum = 1
    var searchedMovies = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movieList = moviesInfo.results
        if let tmp = defauts.array(forKey: "searchedMovies") {
            searchedMovies = tmp as! [String]
        }
        if(searchedMovies.count >= 10) {
            searchedMovies.remove(at: 0)
        }
        var index = 0
        for movie in searchedMovies {
            if searchedText == movie {
                searchedMovies.remove(at: index)
                break
            }
            index += 1
        }
        searchedMovies.append(searchedText)
        defauts.set(searchedMovies, forKey: "searchedMovies")
        movieTable.reloadData()
        
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieTableCell", for: indexPath) as! TableViewCell
        let movie = movieList[indexPath.row]
        cell.titleField.text = movie.title
        cell.dateField.text = movie.releaseDate
        if(movie.posterPath != nil ){
            cell.imageTableView?.load(url: URL(string: "http://image.tmdb.org/t/p/w92\(movie.posterPath!)")!)
        } else {
            cell.imageTableView!.image = no_image_avabile
        }
        cell.owerview.text = movie.overview
        
        return cell
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) {
            pageNum += 1
            if(pageNum <= moviesInfo.totalPages){
                MovieManager(movie: searchedText, page: pageNum).getUserInfo { result in
                    switch result {
                    case .failure(let error):
                        print(error)
                    case .success(let res):
                        self.addNewPage(newMoviesList: res.results)
                    }
                }
            }
            
        }
    }
    
    
    func addNewPage(newMoviesList: [Results]) {
        DispatchQueue.main.async {
            self.movieList += newMoviesList
            self.tableView.reloadData()
        }
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
