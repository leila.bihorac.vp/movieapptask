//
//  TableViewCell.swift
//  MovieAppTask
//
//  Created by Leila Bihorac on 07/05/2020.
//  Copyright © 2020 Leila Bihorac. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet var imageTableView: UIImageView!
    
    @IBOutlet weak var titleField: UILabel!
    @IBOutlet weak var dateField: UILabel!
    @IBOutlet weak var owerview: UILabel!
    
}
