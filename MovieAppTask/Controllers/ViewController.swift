//
//  ViewController.swift
//  MovieAppTask
//
//  Created by Leila Bihorac on 07/05/2020.
//  Copyright © 2020 Leila Bihorac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var movieList : MoviesJSON!
    var exists = false
    var searchedText = String()
    let defauts = UserDefaults()
    var searchedMovies = [String]()
    var clicked = false
    
    @IBOutlet var searchTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! TableController
        destinationVC.searchedText = searchedText
        destinationVC.moviesInfo = movieList
    }
    
    func handleAPIResults(movies: MoviesJSON){
        movieList = movies
        DispatchQueue.main.async {
            if movies.totalResults < 1 {
                let alert = UIAlertController(title: "Search error", message: "No results were found", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: "goToTable", sender: self)
            }
        }
        
    }
    
    
}


//MARK: -Search bar
extension ViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let tmp = defauts.array(forKey: "searchedMovies") {
            searchedMovies = tmp as! [String]
            searchedMovies.reverse()
            searchTable.reloadData()
        }
        
    }
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if(!searchBar.text!.isEmpty){
            searchedText = searchBar.text!.lowercased()
            MovieManager(movie: searchedText, page: 1).getUserInfo { result in
                switch result {
                case .failure(let error):
                    print(error)
                case .success(let res):
                    self.handleAPIResults(movies: res)
                }
            }
            
        } else {
            let alert = UIAlertController(title: "Search", message: "You didn't enter anything", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchedMovies.removeAll()
    }
    
}

//MARK: -Search list

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchTableCell", for: indexPath)
        let movie = searchedMovies[indexPath.row]
        cell.textLabel?.text = movie
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchedText = searchedMovies[indexPath.row]
        MovieManager(movie: searchedText, page: 1).getUserInfo { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let res):
                self.handleAPIResults(movies: res)
            }
        }
        clicked = true
    }
    
    
}

