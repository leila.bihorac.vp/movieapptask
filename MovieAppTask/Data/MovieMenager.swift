//
//  MovieMenager.swift
//  MovieAppTask
//
//  Created by Leila Bihorac on 07/05/2020.
//  Copyright © 2020 Leila Bihorac. All rights reserved.
//


import Foundation
import CoreData
enum MovieError:Error{
    case noDataAvabile
    case canNotProcessData
}

class MovieManager{
    let searchedMovie : String
    let page : Int
    
    //static let shared = MovieManager(movie: String?)
    init(movie : String, page: Int){
        self.searchedMovie = movie.removingWhitespaces()
        self.page = page
    }
    
    func getUserInfo(completion: @escaping(Result<MoviesJSON, MovieError>) -> Void){
        
        let resourceURL = URL(string: "http://api.themoviedb.org/3/search/movie?api_key=2696829a81b1b5827d515ff121700838&query=\(searchedMovie)&page=\(page)")
        let dataTask = URLSession.shared.dataTask(with: resourceURL!) { (data, _, _) in
            guard let jsonData = data else {
                completion(.failure(.noDataAvabile))
                return
            }
            do{
                let resultMovies = try JSONDecoder().decode(MoviesJSON.self, from: jsonData)
                completion(.success(resultMovies))
            } catch{
                completion(.failure(.canNotProcessData))
            }
        }
        
        dataTask.resume()
        
    }
    
}

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined(separator: "-")
    }
}

