//
//  MoviesJSON.swift
//  MovieAppTask
//
//  Created by Leila Bihorac on 07/05/2020.
//  Copyright © 2020 Leila Bihorac. All rights reserved.
//

//   let moviesJSON = try? newJSONDecoder().decode(MoviesJSON.self, from: jsonData)

import Foundation

// MARK: - MoviesJSON
struct MoviesJSON: Codable {
    let page, totalResults, totalPages: Int
    let results: [Results]

    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}

// MARK: - Results
struct Results: Codable {
    let popularity: Double
    let voteCount: Int
    let video: Bool
    let posterPath: String?
    let id: Int
    let adult: Bool
    let backdropPath: String?
    let originalLanguage: String
    let originalTitle: String
    let genreIDS: [Int]
    let title: String
    let voteAverage: Double
    let overview : String?
    let releaseDate: String?

    enum CodingKeys: String, CodingKey {
        case popularity
        case voteCount = "vote_count"
        case video
        case posterPath = "poster_path"
        case id, adult
        case backdropPath = "backdrop_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case genreIDS = "genre_ids"
        case title
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
    }
}
